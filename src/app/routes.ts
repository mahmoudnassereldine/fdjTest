import { Routes } from '@angular/router';
import { TeamDetailsComponent } from './components/team-details/team-details.component';
import { HomeComponent } from './components/home/home.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
export const routes: Routes = [
  { path: 'team-details/:teamName', component: TeamDetailsComponent },
  { path: 'home', component: HomeComponent },
  { path: '', component: HomeComponent },
  { path: '**', component: PageNotFoundComponent },
];
