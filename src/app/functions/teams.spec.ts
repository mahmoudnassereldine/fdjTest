import { Team } from '../interfaces/team.interface';
import { sortByNonAlphabeticalOrder, removeOddIndex } from './teams';

describe('nonAlphabeticalOrder', () => {
  it('should return  non alphabetical order when there is no capital', () => {
    const teams: Team[] = [
      { name: 'a', badge: '' },
      { name: 'b', badge: '' },
      { name: 'c', badge: '' },
    ];
    const result = teams.sort(sortByNonAlphabeticalOrder);
    const expectedResult: Team[] = [
      { name: 'c', badge: '' },
      { name: 'b', badge: '' },
      { name: 'a', badge: '' },
    ];
    expect(result).toEqual(expectedResult);
  });

  it('should return non alphabetical order when there are capital letters', () => {
    const teams: Team[] = [
      { name: 'a', badge: '' },
      { name: 'b', badge: '' },
      { name: 'B', badge: '' },
      { name: 'c', badge: '' },
    ];
    const result = teams.sort(sortByNonAlphabeticalOrder);
    const expectedResult: Team[] = [
      { name: 'c', badge: '' },
      { name: 'b', badge: '' },
      { name: 'B', badge: '' },
      { name: 'a', badge: '' },
    ];
    expect(result).toEqual(expectedResult);
  });
});

describe('removeOddIndex', () => {
  it('should return 2 teams when we 5 teams', () => {
    const teams: Team[] = [
      { name: 'a', badge: '' },
      { name: 'b', badge: '' },
      { name: 'B', badge: '' },
      { name: 'c', badge: '' },
    ];
    const result = teams.filter(removeOddIndex);
    const expectedResult: Team[] = [
      { name: 'a', badge: '' },
      { name: 'B', badge: '' },
    ];
    expect(result).toEqual(expectedResult);
  });
  it('should return 3 teams when we 6 teams', () => {
    const teams: Team[] = [
      { name: 'a', badge: '' },
      { name: 'b', badge: '' },
      { name: 'B', badge: '' },
      { name: 'c', badge: '' },
      { name: 'd', badge: '' },
    ];
    const result = teams.filter(removeOddIndex);
    const expectedResult: Team[] = [
      { name: 'a', badge: '' },
      { name: 'B', badge: '' },
      { name: 'd', badge: '' },
    ];
    expect(result).toEqual(expectedResult);
  });
});
