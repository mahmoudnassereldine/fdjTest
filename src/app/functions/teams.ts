import { Team } from '../interfaces/team.interface';

//Sort predicate
export const sortByNonAlphabeticalOrder = (a: Team, b: Team) => {
  if (a.name.toUpperCase() < b.name.toUpperCase()) {
    return 1;
  }
  if (a.name.toUpperCase() > b.name.toUpperCase()) {
    return -1;
  }
  return 0;
};

//Filter predicate
export const removeOddIndex = (team: Team, index: number): boolean => {
  return index % 2 == 0 ? true : false;
};
