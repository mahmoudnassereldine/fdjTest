import {
  Component,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { Team } from '../../../app/interfaces/team.interface';
import {
  sortByNonAlphabeticalOrder,
  removeOddIndex,
} from '../../functions/teams';
import { SportsDbService } from '../../services/sports-db.service';
@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss'],
})
export class TeamsComponent implements OnInit, OnDestroy {
  @Input() league!: string;
  teams: Team[];
  subscription: Subscription;
  constructor(private sportsDbService: SportsDbService) {
    this.teams = [];
    this.subscription = new Subscription();
  }

  ngOnInit(): void {
    this.loadTeams();
  }

  loadTeams(): void {
    this.subscription = this.sportsDbService
      .loadTeamsByLeague(this.league)
      .subscribe(
        (teams) =>
          (this.teams = teams
            .sort(sortByNonAlphabeticalOrder)
            .filter(removeOddIndex))
      );
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
