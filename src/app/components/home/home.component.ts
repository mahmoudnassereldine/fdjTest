import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { map, Observable, Subscription } from 'rxjs';
import { League } from '../../interfaces/leagues.interface';
import { SportsDbService } from '../../services/sports-db.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  myControl: FormControl;
  leagues: League[];
  subscription: Subscription;
  filteredOptions: Observable<League[]>;
  isTeamsVisible: boolean;
  constructor(
    private sportsDbService: SportsDbService,
  ) {
    this.leagues = [];
    this.subscription = new Subscription();
    this.myControl = new FormControl();
    this.filteredOptions = new Observable<League[]>();
    this.isTeamsVisible = false;
  }
  ngOnInit(): void {
    this.loadLeagues();
    // Auto destroyed by the template pipe
    this.filteredOptions = this.myControl.valueChanges.pipe(
      map((value) => {
        this.hideTeams();
        return value ? this._filter(value) : ([] as League[]);
      })
    );
  }
  //Load all leagues
  loadLeagues(): void {
    this.subscription = this.sportsDbService
      .loadLeagues()
      .subscribe((leagues) => (this.leagues = leagues));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  showTeams(): void {
    this.isTeamsVisible = true;
  }
  hideTeams(): void {
    this.isTeamsVisible = false;
  }

  private _filter(value: string): League[] {
    const filterValue = value.toLowerCase();
    return this.leagues.filter((option) =>
      option.name.toLowerCase().includes(filterValue)
    );
  }
}
