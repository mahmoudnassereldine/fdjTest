import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { TeamDetails } from '../../interfaces/teams-details.interface';
import { SportsDbService } from '../../services/sports-db.service';

@Component({
  selector: 'app-team-details',
  templateUrl: './team-details.component.html',
  styleUrls: ['./team-details.component.scss'],
})
export class TeamDetailsComponent implements OnInit, OnDestroy {
  teamDetails?: TeamDetails;
  teamName: string | null;
  subscription: Subscription;
  constructor(
    private sportsDbService: SportsDbService,
    private activatedroute: ActivatedRoute
  ) {
    this.teamName = this.activatedroute.snapshot.paramMap.get('teamName');
    this.subscription = new Subscription();
  }

  ngOnInit(): void {
    if (this.teamName) {
      this.subscription = this.sportsDbService
        .loadTeamDetailsByTeamName(this.teamName)
        .subscribe((teamDetails) => (this.teamDetails = teamDetails));
    }
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
