import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import {
  createTeamDetailsFromJson,
  TeamDetails,
} from '../interfaces/teams-details.interface';
import { createTeamsFromJson, Team } from '../interfaces/team.interface';
import { createLeaguesFromJson, League } from '../interfaces/leagues.interface';
import { BASE_URL } from '../const';

@Injectable({
  providedIn: 'root',
})
export class SportsDbService {
  constructor(private httpClient: HttpClient) {}

  loadLeagues(): Observable<League[]> {
    return this.httpClient
      .get(BASE_URL + 'all_leagues.php')
      .pipe(map((response) => createLeaguesFromJson(response)));
  }
  loadTeamsByLeague(league: string): Observable<Team[]> {
    return this.httpClient
      .get(BASE_URL + 'search_all_teams.php', {
        params: { l: league },
      })
      .pipe(map((response) => createTeamsFromJson(response)));
  }
  loadTeamDetailsByTeamName(teamName: string): Observable<TeamDetails> {
    return this.httpClient
      .get(BASE_URL + 'searchteams.php', {
        params: { t: teamName },
      })
      .pipe(map((response) => createTeamDetailsFromJson(response)));
  }
}
