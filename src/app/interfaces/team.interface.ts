export interface Team {
  name: string;
  badge: string;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const createTeamsFromJson = (json: any): Team[] => {
  const teams = json.teams;
  const result: Team[] = [];
  for (let index = 0; index < teams.length; index++) {
    const tempLeague = teams[index];
    const team: Team = {
      name: tempLeague.strTeam,
      badge: tempLeague.strTeamBadge,
    };
    result.push(team);
  }
  return result;
};
