export interface League {
  name: string;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const createLeaguesFromJson = (json: any): League[] => {
  const leagues = json.leagues;
  const result: League[] = [];
  for (let index = 0; index < leagues.length; index++) {
    const league: League = { name: leagues[index].strLeague };
    result.push(league);
  }
  return result;
};
