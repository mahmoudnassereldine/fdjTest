export interface TeamDetails {
  teamName: string;
  teamBanner: string;
  country: string;
  leagueName: string;
  englishDescription: string;
  stadiumDescription: string;
}


// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const createTeamDetailsFromJson = (json: any): TeamDetails => {
  const details = json.teams[0];
  const result: TeamDetails = {
    teamName: details.strTeam,
    teamBanner: details.strTeamBanner,
    country: details.strCountry,
    leagueName: details.strLeague,
    englishDescription: details.strDescriptionEN,
    stadiumDescription: details.strStadiumDescription,
  };

  return result;
};
